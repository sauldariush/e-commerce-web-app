// Bootstrap grid system components (row, col)

import { Fragment } from "react";
import { Container, Row, Col, Image, Card, CardGroup, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Home() {

    return (
        <>  
        <Fragment className="px-5" >
            <Container fluid style={{  
                backgroundImage: "linear-gradient(rgba(0, 0, 0, 0.527),rgba(0, 0, 0, 0.5)), url(" + "https://res.cloudinary.com/dlk7w16gw/image/upload/v1679688002/wngt11n69rglnysstf5b.jpg" + ")",
                backgroundPosition: 'center',
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat',
                margin: '0',
                height: '70vh',
                
                }} className="px-5 ">
                <Row className="p-5 px-5 mx-5">
                    
                    <Col className=" mt-5 p-5 px-5 mx-5" style={{ marginBottom: "5rem", textAlign: "left" }} >

                        <h3 className="text-left mt-3 text-light font-sub-header-line1">We are</h3>
                        <h1 className="mt-3 text-light font-main-header">RANDOM THINGS</h1>
                        <p className="pb-3 mt-3 mr-5 pr-5 text-light font-sub-header-line2" >
                            we bring happiness and color to the lives of our beloved customers! Our mission is to make your shopping experience as enjoyable and vibrant as possible, by offering a wide selection of high-quality products that will bring joy to your day-to-day life. Our team is dedicated to providing you with exceptional customer service, and we take pride in bringing you products that are not only beautiful, but also affordable. We believe that everyone deserves a little bit of happiness in their lives, and we are here to help you find it!
                        </p>
                        <Button className="px-5 button" variant="success" style={{ marginBottom: "5rem" }} size="lg" as={Link} to='/products'>Get Started</Button>
                        
                    </Col>
                </Row>
            </Container>

        </Fragment>

            <Container fluid>
                <CardGroup >
                    <Card className=" m-5 border rounded forms">
                        <Card.Img variant="top" src="https://res.cloudinary.com/dlk7w16gw/image/upload/v1679677986/p2jflotc69o9ania8keo.png"/>
                        <Card.Body>
                        <Card.Title className="font-subtitle">Exceptional User Experience</Card.Title>
                        <Card.Text className="font-text">
                        A user-friendly interface facilitates easy navigation, quick access to product information, and a hassle-free checkout process. It should prioritize the user's needs and convenience, ensuring a positive shopping experience.
                        </Card.Text>
                        </Card.Body>
                        
                    </Card>
                    <Card className=" m-5 border rounded forms">
                        <Card.Img variant="top" src="https://res.cloudinary.com/dlk7w16gw/image/upload/v1679678252/vllibybkbcsgsykwjhee.png" />
                        
                        <Card.Body>
                        <Card.Title className="font-subtitle">Fast Transaction</Card.Title>
                        <Card.Text className="font-text">
                        For a smooth and hassle-free shopping experience, it is important to provide customers with simple payment methods, dependable and speedy delivery services, and responsive admins who can assist with any inquiries or issues that may arise.
                        </Card.Text>
                        </Card.Body>
                        
                    </Card>
                    <Card className=" m-5 border rounded forms">
                        <Card.Img variant="top" src="https://res.cloudinary.com/dlk7w16gw/image/upload/v1679678059/ewen3bxfhq7bgilvfsf4.png" />
                        <Card.Body>
                        <Card.Title className="font-subtitle">Trusted by Clients</Card.Title>
                        <Card.Text className="font-text">
                        Positive reviews and feedback from customers plays a crucial role in building trust and credibility for a brand. They act as social proof, inspiring prospective customers to make a purchase and thus, aiding the growth of the business.
                        </Card.Text>
                        </Card.Body> 
                    </Card>
                </CardGroup>          
            </Container>

            
            <footer className="text-center my-3">
                Terms of Service | Privacy Policy | © 2023 Random Things. All Rights Reserved.
            </footer>
        </>
          
    )
}
