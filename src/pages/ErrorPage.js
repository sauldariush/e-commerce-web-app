import { Container, Image } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function ErrorPage(){

    return(
        <Container fluid className="text-center">

            <Image className="img-fluid" src="https://res.cloudinary.com/dlk7w16gw/image/upload/v1679682761/jreva7x0fx4syyaxjbk5.png" />
            <h1>Seems you are lost</h1>
            <Link to={"/"}>go home</Link>
        </Container>
    )

}