import { useEffect, useState } from 'react';
import { Table } from 'react-bootstrap'
import CartTable from '../components/cartComponents/CartTable';
import { base_url } from '../utils/axios';

export default function Carts(){
    const [ carts, setCarts ] = useState([]);

    useEffect(() => {
        fetch(`${base_url}/users/viewAllCart`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => setCarts(data))
    }, []);

    return (
       
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>User ID</th>
                        <th>Order Details</th>
                        <th>Total Amount</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                {carts.map((cart) => (
                    <tr key={ cart._id }>
                        <CartTable  cart={cart} />
                    </tr>
                ))}
                    
                </tbody>
            </Table>
        
    )
}
