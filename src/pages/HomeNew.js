
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";

const featureList = [ 
    {
        title: "Exceptional User Experience",
        text: "A user-friendly interface facilitates easy navigation, quick access to product information, and a hassle-free checkout process. It should prioritize the user's needs and convenience, ensuring a positive shopping experience."
    },
    {
        title: "Fast Transaction",
        text: "For a smooth and hassle-free shopping experience, it is important to provide customers with simple payment methods, dependable and speedy delivery services, and responsive admins who can assist with any inquiries or issues that may arise."
    },
    {
        title: "Trusted by Clients",
        text: "Positive reviews and feedback from customers plays a crucial role in building trust and credibility for a brand. They act as social proof, inspiring prospective customers to make a purchase and thus, aiding the growth of the business."
    }
]

export default function HomeNew() {


return (
    <Container>
        <Box>
            <Typography variant="h2" sx={{ my:4, textAlign: "center", color: "primaray.main"}}>
                We Are
            </Typography>
            <Typography variant="h1" sx={{ my:4, textAlign: "center", color: "primaray.main"}}>
                Random Things
            </Typography>
            <Typography >
                We offer a diverse selection of amazing products to meet all your needs. Shop from the comfort of your home and enjoy fast shipping and secure payment options. Thank you for choosing us as your shopping destination!
            </Typography>
        </Box>
        <Box sx={{ 
            pt: '4',
            display: 'flex', 
            flexDirection: {xs:'column', md: 'row'},
            justifyContent: 'space-between',
            gap: '4',
            flexWrap: 'wrap'
        }}>
        { featureList.map((feature) => (
            <Paper elevation={3} sx={{width: '30%'}}>
                <Box sx={{ m:'3'}}>
                    <Typography variant="h5" >{feature.title}</Typography>
                    <Typography sx={{ mt:'2'}}>
                        {feature.text}
                    </Typography>
                </Box>
            </Paper>
        ))}
        </Box>
        
    </Container>
 )
}    