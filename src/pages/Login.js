import { useState, useEffect, useContext } from 'react';

import { Form, Button, Card, Row, Col, Container, FloatingLabel } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';
import { base_url } from '../utils/axios';

export default function Login() {

	// Allows us to consume the User context object and it's properties to use for user validation
	const { user, setUser } = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

	function authenticate(e) {
        // Prevents page redirection via form submission
        e.preventDefault();
		// Process a fetch request to the corresponding backend API
		// The header information "Content-Type" is used to specify that the information being sent to the backend will be sent in the form of JSON
		// The fetch request will communicate with our backend application providing it with a stringified JSON
		// Convert the information retrieved from the backend into a JavaScript object using the ".then(res => res.json())"
		// Capture the converted data and using the ".then(data => {})"
		// Syntax
			// fetch('url', {options})
			// .then(res => res.json())
			// .then(data => {})
		fetch(`${base_url}/users/login`, 
		{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)
			// If no user information is found, the "access" property will not be available and will return undefined
			// Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
			if(typeof data.access !== 'undefined'){

				// The JWT will be used to retrieve user information across the the whole frontend application and storing it in the localStorage will allow ease of access to the user's information
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: 'Login successful',
					icon: 'success',
					text: 'Welcome to Random things!'
				})
			} else {

				Swal.fire({
					title: 'Authentication Failed',
					icon: 'error',
					text: 'Check your login credentials and try again.'

				})
			}
		});

        // Set the email of the authenticated user in the local storage
        // Syntax
        	// localStorage.setItem('propertyName', value);
		// Storing information in the localStorage will make the data persistent even as the page is refreshed unlike with the use of states where the information is reset when refreshing the page
        // localStorage.setItem('email', email) -- implemented as jwt

		// Set the global user state to have properties obtained from local storage
        // Though access to the user information can be done via the localStorage this is necessary to update the user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout
		// When states change components are rerendered and the AppNavbar component will be updated based on the user credentials, unlike when using the localStorage where the localStorage does not trigger component rerendering
		// setUser({ email: localStorage.getItem('email')})

        // Clear input fields after submission
        setEmail('');
        setPassword('');

    }

	const retrieveUserDetails = (token) => {
		fetch(`${base_url}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			// The token will be sent as part of the request's header information
			// We put "Bearer" in front of the token to follow implementation standards for JWTs
			setUser({
				id: data._id,
				isAdmin: data.isAdmin

			})
		})
	}
	useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    return (

		( user.id !== null ) ?
			<Navigate to='/'/>
		: 	
			<>
			<Container fluid style={{  
                backgroundImage: "linear-gradient(rgba(0, 0, 0, 0.527),rgba(0, 0, 0, 0.5)), url(" + "https://res.cloudinary.com/dlk7w16gw/image/upload/v1679684189/ynolwvjrsxwxmybngk3f.jpg" + ")",
                backgroundPosition: 'center',
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat',
                margin: '0',
                height: '100vh',
                

                }}>
				<Row className='p-5'>
					<Col xl={{ span: 6, offset: 3}} className='p-5'>
						<Card className='cardHighlight p-5 forms m-5'>
							<Card.Body>
								<Card.Title className='text-center pb-2'>
									<h1 className='font-main-header'>Login</h1>
								</Card.Title>
								<Form onSubmit={e => authenticate(e)}>
									<Form.Group className="mb-3" controlId="userEmail">
										
									</Form.Group>

									<Form.Group className="mb-3" controlId="password">
										<FloatingLabel
											controlId="floatingInputEmail"
											label="Email Address"
											className="mb-3"
										>
											<Form.Control 
												type="email"
												value={email}
												onChange={(e) => setEmail(e.target.value)}
												required
											/>
										</FloatingLabel>

										<FloatingLabel
											controlId="floatingInputPassword"
											label="Password"
											className="mb-3"
										>
											<Form.Control 
												type="password" 
												value={password}
												onChange={(e) => setPassword(e.target.value)}
												required
											/>
										</FloatingLabel>
									</Form.Group>

									<Form.Group className='text-center d-grid'>
									{ isActive ? 
										<Button variant="success" type="submit" id="submitBtn">
											Submit
										</Button>
										: 
										<Button variant="danger" type="submit" id="submitBtn" disabled>
											Submit
										</Button>
									}
									</Form.Group>
									<p className='text-center font-footer m-2'> Not yet registered? <Link to={`/register`}>Click here</Link> to create an account!</p>
								</Form>
								
							</Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>
			</>	
	)
}