import { useState, useEffect } from 'react';
import { Form, Button, Card, Row, Image, Col, Container,  FloatingLabel } from 'react-bootstrap';
import { useNavigate, Link } from 'react-router-dom';

import Swal from 'sweetalert2';
import { base_url } from '../utils/axios';

export default function Register(){

    // State hooks to store the values of the input fields
    // getters are variables that store data (from setters)
	// setters are function that sets the data (for the getters)
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNum, setMobileNum] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    const navigate = useNavigate();

    // Function to simulate user registration
    function registerUser(e) {
        // Prevents page redirection via form submission
        e.preventDefault();

            fetch(`${base_url}/users/register`, 
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        mobileNum: mobileNum,
                        email: email,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {
                    if(data){
                        console.log(data)
                        Swal.fire({
                            title: 'Registration successful',
                            icon: 'success',
                            text: 'You have successfully registered an account'
                        })
                        navigate("/login");
                        
                    } else {
                        Swal.fire({
                            title: 'Duplicate Email',
                            icon: 'error',
                            text: 'Please use different email.'
        
                        })
                    }                  
                })

        
        // Clear input fields
        setFirstName("");
        setLastName("");
        setMobileNum("");
        setEmail("");
        setPassword1("");
        setPassword2("");   
    }

    useEffect(() => {
        // Validation to enable submit button when all fields are populated and both passwords match
        if((firstName !== '' && lastName !== '' && email !== '' && mobileNum !== '' && password1 !== '' && password2 !=='') && (password1 === password2) && (mobileNum.length === 11)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [firstName, lastName, mobileNum, email, password1, password2]);

    return(

        <>  
        <Container fluid style={{  
                backgroundImage: "linear-gradient(rgba(0, 0, 0, 0.527),rgba(0, 0, 0, 0.5)), url(" + "https://res.cloudinary.com/dlk7w16gw/image/upload/v1679684813/z9yryo2837ndgdvfqa7w.jpg" + ")",
                backgroundPosition: 'center',
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat',
                margin: '0',
                height: '100vh',
                

                }}>
            <Row>
                
                <Col xl={{ span: 6, offset: 3}}>
                    <Card className='cardHighlight p-5 forms m-5'>
                    <Card.Body>
                        <Card.Title className='text-center pb-2'>
                            <h1 className='font-main-header'>Register</h1>
                        </Card.Title>
                            {/* // 2-way Binding (Binding the user input states into their corresponding input fields via the onChange JSX event Handler) */}
                        <Form onSubmit={(e) => registerUser(e)}>

                            <FloatingLabel
                                controlId="floatingInputFirstName"
                                label="First Name"
                                className="mb-3"
                            >
                                <Form.Control 
                                type='first name'
                                value={ firstName }
                                onChange={ e => setFirstName(e.target.value)} 
                                required/>
                            </FloatingLabel>

                            <FloatingLabel
                                controlId="floatingInputLastName"
                                label="Last Name"
                                className="mb-3"
                            >
                                <Form.Control 
                                type='last name'
                                value={ lastName }
                                onChange={ e => setLastName(e.target.value)} 
                                required/>
                            </FloatingLabel>

                            <FloatingLabel
                                controlId="floatingInputMobileNum"
                                label="Mobile Number"
                                className="mb-3"
                            >
                                <Form.Control 
                                type='mobile number'
                                value={ mobileNum }
                                onChange={ e => setMobileNum(e.target.value)} 
                                required/>
                            </FloatingLabel>

                            <FloatingLabel
                                controlId="floatingInputEmail"
                                label="Email Address"
                                className="mb-3"
                            >
                                <Form.Control 
                                type='email'
                                value={ email }
                                onChange={ e => setEmail(e.target.value)} 
                                required/>
                            
                            </FloatingLabel>
                                <Form.Text className='text-muted nav-font'>
                                    We'll never share your email with anyone else.
                                </Form.Text>

                            <FloatingLabel
                                controlId="floatingInputPassword1"
                                label="Password"
                                className="mb-3"
                            >
                                <Form.Control 
                                type='password'
                                value={ password1 }
                                onChange={ e => setPassword1(e.target.value)} 
                                required/>
                            </FloatingLabel>

                            <FloatingLabel
                                controlId="floatingInputPassword2"
                                label="Verify password"
                                className="mb-3"
                            >
                                <Form.Control 
                                type='password'
                                value={ password2 }
                                onChange={ e => setPassword2(e.target.value)} 
                                required/>
                            </FloatingLabel>
                            

                            <Form.Group className='text-center mb-3 d-grid'>
                            { isActive ?
                                <Button variant='primary' type='submit' id='submitBtn'>
                                    Submit
                                </Button>
                                :
                                <Button variant='danger' type='submit' id='submitBtn' disabled>
                                    Submit
                                </Button>
                            } 
                            </Form.Group>
                            
                            
                        </Form>
                        <p className='text-center font-footer'> Already registered? <Link to={`/login`}>Click here</Link> to login</p> 
                    </Card.Body>
                    </Card>
                                      
                    
                </Col>
            </Row>

        </Container>
            
        </>
        
            
    )
};