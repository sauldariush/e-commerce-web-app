import { useEffect, useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import ProductCard from '../components/productsComponents/ProductCard';
import { base_url } from '../utils/axios';

export default function Products(){

    
    const [ products, setProducts ] = useState([]);

    useEffect(() => {

        fetch(`${base_url}/products/activeProducts`)
        .then(res => res.json())
        .then(data => {
            setProducts(data);
        })
        
    }, []);


    return (
        <>
            <h1 className='text-center p-5 m-2 font-sub-header-line1'>Choose anything that best suits your needs</h1>
            <Container className="justify-content-md-center">
                <Row >
                    {products.map(product => (
                        <Col className='my-3' sm={12} md={6} lg={4} xl={3} key={product._id}>
                            <ProductCard product={product} />
                        </Col>
                    ))}
                </Row>
            </Container>
        </>
    )
}