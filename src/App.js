import { useState, useEffect } from 'react';
// downloaded package module imports
import { Container } from 'react-bootstrap';

import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import './App.css';

import AppNavbar from './components/appComponents/AppNavbar'
import AdminDashboard from './pages/AdminDashboard';
import ErrorPage from './pages/ErrorPage';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Products';
import OrderHistory from './components/cartComponents/OrderHistory';
import ProductView from './components/productsComponents/ProductView';
import Register from './pages/Register';
import UpdateProduct from './components/productsComponents/UpdateProduct';
import UpdateUserProfile from './components/userComponents/UpdateUserProfile';
import { base_url } from './utils/axios';

import { UserProvider } from './UserContext';

function App() {
  // State hook for the user state that's defined here for a global scope
  // Initialized as an object with properties from the localStorage
  // This will be used to store the user information and will be used for validating if a user is logged in on the app or not
  const [ user, setUser ] = useState({ 
    id: null,
    isAdmin: null
  });

  //Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  // Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    fetch(`${base_url}/users/details`,{
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    });
  }, [])

  return (

    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
            {/*The `Routes` component holds all our Route components. It selects which `Route` component to show based on the URL Endpoint. For example, when the `/courses` is visited in the web browser, React.js will show the `Courses` component to us.*/}
            <Routes>

              <Route path='/' element={<Home />}/>
              <Route path='/login' element={<Login />}/>
              <Route path='/logout' element={<Logout />}/>
              <Route path='/updateProfile' element={<UpdateUserProfile />}/>
              <Route path='/register' element={<Register />}/>
              <Route path='*' element={<ErrorPage />}/>

              {(user.isAdmin) ?
              <>
                <Route path='/adminDashboard' element={<AdminDashboard />}/>
                <Route path='/update/:productId' element={<UpdateProduct />}/>
                
              </> 
              :
              <>
                <Route path='/orderHistory'element={<OrderHistory />}/>
                <Route path='/products' element={<Products />}/>
                <Route path='/products/:productId' element={<ProductView />}/>
                
              </>
              }

            </Routes>
          
      </Router>
    </UserProvider>
  );
}

export default App;
