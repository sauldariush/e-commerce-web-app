import { useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import { Link, useNavigate } from 'react-router-dom';
import Swal from "sweetalert2";

import { base_url } from "../../utils/axios";

function ProductTable({ product }) {

    const navigate = useNavigate();

   
    const { _id, productName, description, price, isActive } = product;

    function activate(){
        fetch(`${base_url}/${_id}/activate`, 
        {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productId: _id

            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data){
                Swal.fire({
                    title: 'Product activate successful',
                    icon: 'success',
                    text: 'You have successfully activated a product!'
                })
                product.isActive = true;
                navigate("/adminDashboard");
                
            } else {
                Swal.fire({
                    title: 'Error updating',
                    icon: 'error',
                    text: 'Please input data.'

                })
            }                  
        })

    }

    function archive(){
        fetch(`${base_url}/${_id}/archive`, 
        {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productId: _id

            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data){
                Swal.fire({
                    title: 'Product archive successful',
                    icon: 'success',
                    text: 'You have successfully archived a product!'
                })
                product.isActive = false;
                navigate("/adminDashboard");
                
            } else {
                Swal.fire({
                    title: 'Error updating',
                    icon: 'error',
                    text: 'Please input data.'

                })
            }                  
        })

    }

    useEffect(() => {
        navigate('/adminDashboard');
 
    }, [ isActive ])

    return (

          <>
            <td>{ _id }</td>
            <td>{ productName }</td>
            <td>{ description }</td>
            <td>{ price }</td>
            <td>{ isActive ? "Active" : "Inactive" }</td>
            <td>
                <Link className="btn btn-warning" to={`/update/${_id}`}>Update</Link>
                <br></br>
                { !isActive ?
                    <Link className="btn btn-success" onClick={activate}>Activate</Link>
                :
                    <Link className="btn btn-secondary" onClick={archive}>Disable</Link>
                }
            </td>
          </>
    );
  }
  
  export default ProductTable;