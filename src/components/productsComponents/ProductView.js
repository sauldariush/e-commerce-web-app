import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Image } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../../UserContext';
import { base_url } from '../../utils/axios';

export default function ProductView() {

	// The "useParams" hook allows us to retrieve the courseId passed via the URL
	const { productId } = useParams();

	const { user } = useContext(UserContext);

	// Allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
	//an object with methods to redirect the user
	const navigate = useNavigate();

	const [ productName, setProductName ] = useState("");
	const [ description, setDescription ] = useState("");
	const [ imageUrl, setImageUrl ] = useState("");
	const [ price , setPrice ] = useState(0);
	const [ quantity , setQuantity ] = useState(0);
	const [ disabled , setDisabled ] = useState(false);

	const addToCart = (productId) => {
		fetch(`${base_url}/users/addItemToCart`,
		{
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: user.id,
				productId: productId,
                productName: productName,
				imageUrl: imageUrl,
                price: price,
                quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if (data === true) {

				Swal.fire({
					title: "Successfully added",
					icon: 'success',
					text: "You have successfully this product to your cart."
				});

				// The navigate hook will allow us to navigate and redirect the user back to the courses page programmatically instead of using a component.
				navigate("/products");

			} else {

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}
		});
	}


    function add(){
        setDisabled(false)
        setQuantity(quantity+1);
    } 

    function minus(){
        if(quantity <= 0){
            setDisabled(true)
        } else {
            setDisabled(false)
            setQuantity(quantity-1);
        }      
    } 

	const buyNow = (productId) => {
		fetch(`${base_url}/users/buyNow`,
		{
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: user.id,
				productId: productId,
                productName: productName,
				imageUrl: imageUrl,
                price: price,
                quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if (data === true) {

				Swal.fire({
					title: "Check out Success!",
					icon: 'success',
					text: "You have successfully checked out this product."
				});

				// The navigate hook will allow us to navigate and redirect the user back to the courses page programmatically instead of using a component.
				navigate("/products");

			} else {

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}
		});
	}

	useEffect(() => {

		console.log(productId);

		fetch(`${base_url}/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setProductName(data.productName);
			setDescription(data.description);
			setImageUrl(data.imageUrl);
			setPrice(data.price);
			
		})

	}, [ productId ]);

	return (

		<Container fluid style={{  
			backgroundImage: "linear-gradient(rgba(0, 0, 0, 0.527),rgba(0, 0, 0, 0.5)), url(" + imageUrl + ")",
			backgroundPosition: 'center',
			backgroundSize: 'cover',
			backgroundRepeat: 'no-repeat',
			margin: '0',
			height: '100vh',
			
			}}>
				<Row className='py-5'>
					<Col xl={{ span: 8, offset: 2}} className='py-5 my-5'>
						<Card className='forms'>
							
							<Row >
								<Col fluid>

									<Image fluid style={{
										objectFit: 'contain'
										}} 
										className='image-fluid' 
										src={imageUrl} />
								</Col>
								<Col>
									<Card.Body>
										<Card.Title className='font-subtitle m-0' style={{fontSize: '2rem' }}>{ productName }</Card.Title>
										<Card.Title className='font-deets'>Product Description</Card.Title>
										<Card.Text className='font-deets px-3'>{ description }</Card.Text>
											<br/>
											<br/>
										<Card.Text as='h5' className='text-warning font-subtitle-prod'>₱ {parseFloat(price).toLocaleString()}</Card.Text>
										<Card.Subtitle className='font-deets'>Quantity</Card.Subtitle>
										<Card.Text className='my-3'>
											<Button variant="secondary" size='sm' disabled={disabled} onClick={minus}>-</Button>
											<Card.Text className='btn btn-light m-0 border font-deets'>{quantity}</Card.Text>
											<Button variant="secondary" size='sm' onClick={add}>+</Button>
											<strong className='font-subtitle-prod'> Subtotal:  ₱{parseFloat(quantity*price).toLocaleString()} </strong>
										</Card.Text>
										<Card.Text>
										{ user.id !== null ? 
											<Row  className='d-grid'>
												<Col>
												<Button className='mx-2' variant="primary" block onClick={() => addToCart(productId)}>Add to Cart</Button>
												<Button className='mx-2' variant="primary" block onClick={() => buyNow(productId)}>Buy Now</Button>
												</Col>
												
											</Row>
											
										: 
											<Link className="btn btn-danger btn-block" to="/login">Log in to add to cart</Link>
										}
		
										</Card.Text>
										
										</Card.Body>
								</Col>
							</Row>
						</Card>						
					</Col>
				</Row>
		</Container>
	)

}