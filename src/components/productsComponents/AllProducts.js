import { useEffect, useState } from 'react';
import { Table } from 'react-bootstrap';
import ProductTable from './ProductTable';
import { base_url } from '../../utils/axios';

export default function AllProducts(){
    
    const [ products, setProducts ] = useState([]);

    useEffect(() => {
        fetch(`${base_url}/products/allProducts`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => setProducts(data))
    }, [ products ]);

    return (
        <Table striped bordered hover>
            <thead>
                <tr>
                    <th>Product ID</th>
                    <th>Product Name</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Availability</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                {products.map(product => (
                    <tr key={product._id}>
                        <ProductTable product={product} />
                    </tr>
                ))}
            </tbody>
        </Table>
    )
}
