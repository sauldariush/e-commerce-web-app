import { useState, useEffect } from 'react';
import { Form, Button, Card } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';
import { base_url } from '../../utils/axios';

export default function CreateProduct(){

    // State hooks to store the values of the input fields
    // getters are variables that store data (from setters)
	// setters are function that sets the data (for the getters)
    const [productName, setProductName] = useState('');
    const [description, setDescription] = useState('');
    const [imageUrl, setImageUrl] = useState('');
    const [price, setPrice] = useState(0);

    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    const navigate = useNavigate();

    // Function to simulate user registration
    function createProduct(e) {
        // Prevents page redirection via form submission
        e.preventDefault();

            fetch(`${base_url}/products/create`, 
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        "Authorization": `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
                        productName: productName,
                        description: description,
                        price: price,
                        imageUrl: imageUrl
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)
                    if(data){
                        Swal.fire({
                            title: 'Registration successful',
                            icon: 'success',
                            text: 'You have successfully added a product!'
                        })
                        navigate("/adminDashboard");
                        
                    } else {
                        Swal.fire({
                            title: 'Duplicate Product',
                            icon: 'error',
                            text: 'Please use different email.'
        
                        })
                    }                  
                })

        
        // Clear input fields
        setProductName("");
        setDescription("");
        setImageUrl("");
        setPrice(0);
 
    }

    useEffect(() => {
        // Validation to enable submit button when all fields are populated and both passwords match
        if(productName !== '' && description !== '' && imageUrl !== '' && price !== '' && price !== 0 && !isNaN(price)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [ productName, description, price, imageUrl ]);

    return(
        <Card className='cardHighlight p-5'>
            <Card.Body>
                <Card.Title className='text-center'>
                    <h1 className='font-sub-header-line1'>Create a Product</h1>
                </Card.Title>
                    {/* // 2-way Binding (Binding the user input states into their corresponding input fields via the onChange JSX event Handler) */}
                <Form onSubmit={(e) => createProduct(e)}>
                    <Form.Group className='mb-3' controlId='productName'>
                        <Form.Label>Product Name</Form.Label>
                        <Form.Control 
                            type='text' 
                            placeholder='Enter product name'
                            value={ productName }
                            onChange={ e => setProductName(e.target.value)} 
                            required/>
                    </Form.Group>

                    <Form.Group className='mb-3' controlId='productDescription'>
                        <Form.Label>Description</Form.Label>
                        <Form.Control 
                            type='text' 
                            placeholder='Enter description'
                            value={ description }
                            onChange={ e => setDescription(e.target.value)} 
                            required/>
                    </Form.Group>

                    <Form.Group className='mb-3' controlId='productImageUrl'>
                        <Form.Label>Image URL</Form.Label>
                        <Form.Control 
                            type='text' 
                            placeholder='Enter Image URL'
                            value={ imageUrl }
                            onChange={ e => setImageUrl(e.target.value)} 
                            required/>
                    </Form.Group>

                    <Form.Group className='mb-3' controlId='productPrice'>
                        <Form.Label>Price</Form.Label>
                        <Form.Control 
                            type='price' 
                            placeholder='Enter product price'
                            value={ price }
                            onChange={ e => setPrice(e.target.value)} 
                            required/>
                    </Form.Group>

                    { isActive ?
                        <Button variant='primary' type='submit' id='submitBtn'>
                            Submit
                        </Button>
                        :
                        <Button variant='danger' type='submit' id='submitBtn' disabled>
                            Submit
                        </Button>
                    }
                </Form>
            </Card.Body>
            
        </Card>
            
    )
};