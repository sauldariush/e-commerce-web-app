import React from "react";
import { useContext, useState, useEffect } from "react";
import { Table, Button, Row, Col } from "react-bootstrap";
import Swal from "sweetalert2";

import UserContext from '../../UserContext';
import { base_url } from "../../utils/axios";

export default function MyCart() {

    const [myCart, setMyCart] = useState([]);
    const { user } = useContext(UserContext);
    function deleteItem(productName){
        fetch(`${base_url}/users/deleteItemOnCart`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }, 
            body: JSON.stringify({
                cartId: _id,
                productName: productName
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data === true) {
                Swal.fire({
                    title: "Item Removed",
                    icon: 'success',
                    text: "You have removed this product to your cart."
                })
                            
            } else {
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again."
                });
            }
        });
    }
    const { _id, products, totalAmount, status } = myCart;
    function checkOutCart(){
        fetch(`${base_url}/users/${user.id}/checkOut`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                cartId: _id
            })
            
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                setMyCart([]);
				Swal.fire({
					title: "Check out successful",
					icon: 'success',
					text: "You have checked out your cart."
				})
                
            } else {
                Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
            }
        })

    }
    useEffect(() => {
        fetch(`${base_url}/users/${user.id}/myCart`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setMyCart(data);            
        })
    }, [ user.id, myCart ])
    

    return (
        <>
            {  (!products || products.length === 0 || status === "Check Out")  ?
            <>
                <Row>
                    <Col>
                        <img style={{ width: '25rem', objectFit: 'contain'}} src="https://res.cloudinary.com/dlk7w16gw/image/upload/v1679677023/ak9ty3hztguzugg4oyeg.png"/>
                    </Col>
                    <Col className="py-5 my-5 text-center">
                        <h1>Cart is Empty</h1>
                        <p>Go to products to add items to your cart</p>                       
                    </Col>
                </Row>
                
            </>
            :
            <>          
            <Table responsive hover className="text-center">
            <thead >
                <tr>
                    <th>Image</th>
                    <th>Item Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Subtotal</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody >
                {products && products.map((items) => {
                    return (
                        <tr key={items.id}>
                            <td>
                                <img 
                                    style={{
                                        height: '5rem',
                                        width:'8rem',
                                        objectFit: 'cover'
                                    }}
                                    src={items.imageUrl}
                                />
                            </td>
                            <td>{items.productName}</td>
                            <td>₱ {parseFloat(items.price).toLocaleString()}</td>
                            <td >
                                <Row className="text-center">
                                    <Col >
                                        {items.quantity}
                                    </Col>
                                </Row>
                            </td>
                            <td>₱ {parseFloat(items.subtotal).toLocaleString()}</td>
                            <td>
                                <Button 
                                    variant="outline-danger" 
                                    className="px-3" 
                                    onClick={() => {
                                        
                                        deleteItem(items.productName);
                                    }}
                                >X</Button>
                            </td>
                    </tr>
                    )
                    
                })}   
                <tr >                  
                    <td colSpan={4} ><Button variant="success" className="px-5" onClick={checkOutCart} >Check Out</Button></td>
                    <td colSpan={2} className="text-center"><strong>Total: ₱ {parseFloat(totalAmount).toLocaleString()}</strong></td>
                    
                </tr>
            </tbody>
            </Table>        
            </>        
            }
        </>        
    );
}
