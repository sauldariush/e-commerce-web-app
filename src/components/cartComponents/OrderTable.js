import { Accordion, Table, Row, Col, Container } from "react-bootstrap";

function OrderTable({ order }) {

   
    const { _id, products, totalAmount, purchasedOn } = order;

    return (

          <>

            <Accordion.Header><strong>Order ID: {   _id }</strong> </Accordion.Header>
                    <Accordion.Body className="mx-5">
                        <strong>Products</strong>
                        <Table responsive hover className="text-center">
                            <thead >
                                <tr>
                                    <th>Image</th>
                                    <th>Item Name</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Subtotal</th>
                                    
                                </tr>
                            </thead>
                            <tbody >
                                {products && products.map((items) => {
                                    return (
                                        <tr key={items.id}>
                                            <td>
                                                <img 
                                                    style={{
                                                        height: '5rem',
                                                        width:'8rem',
                                                        objectFit: 'cover'
                                                    }}
                                                    src={items.imageUrl}
                                                />
                                            </td>
                                            <td>{items.productName}</td>
                                            <td>₱ {parseFloat(items.price).toLocaleString()}</td>
                                            <td >
                                                <Row className="text-center">
                                                    <Col >
                                                        {items.quantity}
                                                    </Col>
                                                </Row>
                                            </td>
                                            <td>₱ {parseFloat(items.subtotal).toLocaleString()}</td>
                                    </tr>
                                    )
                                    
                                })}   

                            </tbody>
                        </Table>
                        <Container className="text-end">
                            <strong >Total Amount: ₱ {parseFloat(totalAmount).toLocaleString()}</strong>
                            <p>Date of Purchase: { purchasedOn }</p>
                        </Container>
            </Accordion.Body>
          </>
    );
  }
  
  export default OrderTable;