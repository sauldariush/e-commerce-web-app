
import React from "react";

export default function CartTable({ cart }) {
 
    const  { userId, products, totalAmount, status } = cart;
    
  return (    
        <>
            <td>{userId}</td>
            <td>
                <ul>
                {products.map((product) => (
                    <li key={product.id}>
                    {product.productName} - {product.quantity} pcs : <strong>₱ {parseFloat(product.subtotal).toLocaleString()}</strong>
                    </li>
                ))}
                </ul>
            </td>
            <td><strong>₱ {parseFloat(totalAmount).toLocaleString()} </strong></td>
            <td>{status}</td>
        </>
  );
}