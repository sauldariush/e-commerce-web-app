import { useEffect, useState } from 'react';
import { Table, Container, Accordion, Image, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import OrderTable from './OrderTable';
import { base_url } from '../../utils/axios';


export default function OrderHistory(){
    
    const [ orders, setOrders ] = useState([]);

    useEffect(() => {
        fetch(`${base_url}/users/allUserOrder`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => setOrders(data))
    }, []);

    return (
        <>
            {
                (!orders || orders === null || orders.length ===0) ?
                    <Container fluid className='text-center'>
                        <Image fluid src='https://res.cloudinary.com/dlk7w16gw/image/upload/v1679834984/xz6ueuxkp5z4i9mpvzzk.png'/>
                        <p> See products to order</p>
                        <Button as={Link} to='/products'> Click Here</Button>
                    </Container>

                :

                    <Container fluid >
                        <h1 className='text-center'>Order History</h1>
                        <Accordion className='mx-5'>
                            {orders.map(order => (
                            <Accordion.Item eventKey={order._id}>
                                <OrderTable order={order} />
                            </Accordion.Item>
                            ))}
                        </Accordion>
                    </Container>
            }
        </>
        
    )
}