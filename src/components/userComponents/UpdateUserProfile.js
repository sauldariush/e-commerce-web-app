import { useState, useEffect, useContext } from 'react';
import { Form, Button, Card, FloatingLabel, Row, Col, Container } from 'react-bootstrap';
import { useNavigate, useParams } from 'react-router-dom';

import Swal from 'sweetalert2';
import UserContext from '../../UserContext';
import { base_url } from '../../utils/axios';

export default function UpdateUserProfile(){

    // State hooks to store the values of the input fields
    // getters are variables that store data (from setters)
	// setters are function that sets the data (for the getters)
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNum, setMobileNum] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
 
    const { user } =useContext(UserContext);
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    const navigate = useNavigate();

    // Function to simulate user registration
    const updateProfile = () => {

            fetch(`${base_url}/updateUserProfile`, 
                {
                    method: 'PATCH',
                    headers: {
                        'Content-Type': 'application/json',
                        "Authorization": `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
                        userId: user.id,
                        firstName: firstName,
                        lastName: lastName,
                        mobileNum: mobileNum,
                        password: password1 
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)
                    if(data){
                        Swal.fire({
                            title: 'Update successful',
                            icon: 'success',
                            text: 'Profile has been updated!'
                        })
                        
                    }  else {
                        alert(`update failed`)
                    }                 
                })

        // Clear input fields
        setFirstName('');
        setLastName('');
        setMobileNum('');
        setPassword1('');
        setPassword2('');
        navigate('/');
 
    }

    useEffect(() => {
        // Validation to enable submit button when all fields are populated and both passwords match
        if((firstName !== '' && lastName !== '' && mobileNum !== '' && mobileNum.length === 11 && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [ firstName, lastName, mobileNum, password1, password2 ]);

    return(
        <Container fluid style={{  
            backgroundImage: "linear-gradient(rgba(0, 0, 0, 0.527),rgba(0, 0, 0, 0.5)), url(" + "https://res.cloudinary.com/dlk7w16gw/image/upload/v1679805420/jpypzkdxhqixiv51d2gh.jpg" + ")",
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            margin: '0',
            height: '100vh',
            

            }}>
                <Row className='p-5'>
					<Col xl={{ span: 6, offset: 3}} className='p-5'>
                        <Card className='cardHighlight p-5'>
                            <Card.Body>
                                <Card.Title className='text-center pb-3 mt-0'>
                                    <h1>Update Profile</h1>
                                </Card.Title>
                                    {/* // 2-way Binding (Binding the user input states into their corresponding input fields via the onChange JSX event Handler) */}
                                    <Form onSubmit={updateProfile}>
                                        <FloatingLabel
                                            controlId="floatingInput"
                                            label="First Name"
                                            className="mb-3"
                                        >
                                            <Form.Control 
                                            type='first name'
                                            value={ firstName }
                                            onChange={ e => setFirstName(e.target.value)} 
                                            required/>
                                        </FloatingLabel>

                                        <FloatingLabel
                                            controlId="floatingInput"
                                            label="Last Name"
                                            className="mb-3"
                                        >
                                            <Form.Control 
                                            type='last name'
                                            value={ lastName }
                                            onChange={ e => setLastName(e.target.value)} 
                                            required/>
                                        </FloatingLabel>

                                        <FloatingLabel
                                            controlId="floatingInput"
                                            label="Mobile Number"
                                            className="mb-3"
                                        >
                                            <Form.Control 
                                            type='mobile number'
                                            value={ mobileNum }
                                            onChange={ e => setMobileNum(e.target.value)} 
                                            required/>
                                        </FloatingLabel>

                                        <FloatingLabel
                                            controlId="floatingInput"
                                            label="Password"
                                            className="mb-3"
                                        >
                                            <Form.Control 
                                            type='password'
                                            value={ password1 }
                                            onChange={ e => setPassword1(e.target.value)} 
                                            required/>
                                        </FloatingLabel>

                                        <FloatingLabel
                                            controlId="floatingInput"
                                            label="Verify password"
                                            className="mb-3"
                                        >
                                            <Form.Control 
                                            type='password'
                                            value={ password2 }
                                            onChange={ e => setPassword2(e.target.value)} 
                                            required/>
                                        </FloatingLabel>


                                        <Form.Group className='text-center mb-3 d-grid'>
                                        { isActive ?
                                            <Button variant='primary' type='submit' id='submitBtn'>
                                                Submit
                                            </Button>
                                            :
                                            <Button variant='danger' type='submit' id='submitBtn' disabled>
                                                Submit
                                            </Button>
                                        } 
                                        </Form.Group>
                                    </Form>
                            </Card.Body>   
                        </Card>
                    </Col>
                </Row>
            </Container>
    )
};