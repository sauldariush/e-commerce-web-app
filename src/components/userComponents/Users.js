import React, { useEffect, useState } from 'react';
import { Table } from 'react-bootstrap';
import UserTable from './UserTable';
import { base_url } from '../../utils/axios';

export default function Users() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    fetch(`${base_url}/users/allUser`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        }
    })
      .then((res) => res.json())
      .then((data) => {
        if (Array.isArray(data)) {
          setUsers(data);
        } else {
          console.error('Invalid data:', data);
        }
      })
      .catch((error) => {
        console.error('Error fetching users:', error);
      });
  }, []);

  return (
    <Table striped bordered hover>
      <thead className='text-center'>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Mobile Number</th>
          <th>Email</th>
          <th>Role</th>
        </tr>
      </thead>
      <tbody>
        {users.map((user) => (
          <tr key={user._id}>
            <UserTable user={user} />
          </tr>
        ))}
      </tbody>
    </Table>
  );
}