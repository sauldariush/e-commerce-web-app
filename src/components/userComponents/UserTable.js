import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";

export default function UserTable({ user }){

    const {_id, firstName, lastName, mobileNum, email, isAdmin } = user;

    const navigate = useNavigate();


    useEffect(() => {
        navigate('/adminDashboard');
 
    }, [ isAdmin ])

    return(
        <>
            <td>{ _id }</td>
            <td>{ firstName} {lastName} </td>
            <td>{ mobileNum }</td>
            <td>{ email }</td>
            <td>{ isAdmin ? "Admin" : "User" }</td>
          </>

    )
}