// React bootstrap components
 /* 
    import module name from "filePath"
 */

// import Container from "react-bootstrap/Container";

import { useContext,useEffect } from "react";

import { Container, Navbar, Nav, Modal, Button, Dropdown, DropdownButton, Image, OverlayTrigger,Popover } from "react-bootstrap";
import { Link, NavLink, useNavigate } from 'react-router-dom';

import UserContext from "../../UserContext";
import MyCart from '../cartComponents/MyCart';
import { base_url } from "../../utils/axios";

import React, { useState } from 'react';

export default function AppNavbar(){

    // State to store the user information stored in the login page.
    // const [user, setUser] = useState(localStorage.getItem('email'));
    // console.log(user);

    const { user } = useContext(UserContext);

    const navigate = useNavigate();

    const [show, setShow] = useState(false);

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [mobileNum, setMobileNum] = useState("");


    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    fetch(`${base_url}/users/details`, 
    {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            "Authorization": `Bearer ${localStorage.getItem('token')}`
        }
        
    })
    .then(res => res.json())
    .then(data => {
        setFirstName(data.firstName);
        setLastName(data.lastName);
        setEmail(data.email);
        setMobileNum(data.mobileNum);


    })

    useEffect(() => {
        navigate('/');
        
    }, [ firstName, lastName ]);
    

    return (
        <Navbar bg="light" expand="lg" className="px-5 align-items-center">
            <Container fluid>
                <Navbar.Brand as={ Link } to='/' className="font-logo">RANDOM THINGS</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                    {/*The "as" prop allows components to be treated as if they are a different component gaining access to it's properties and functionalities.*/}
                        <Nav.Link as={ NavLink } to='/' className="nav-font" >Home</Nav.Link>
                        {/*- The "to" prop is used in place of the "href" prop for providing the URL for the page.*/}
                        {/*- The "exact" prop is used to highlight the active NavLink component that matches the URL.*/}
                        { (user.id !== null) ?
                            <>
                                    { (user.isAdmin) ?
                                    <Nav.Link as={ NavLink } to='/adminDashboard' className="nav-font" >Admin Dashboard</Nav.Link>
                                :
                                    <>
                                        <Nav.Link as={ NavLink } to='/products' className="nav-font" >Products</Nav.Link>
                                        <Nav.Link as={ NavLink } onClick={handleShow} className="nav-font" ><Image style={{width: '2rem'}} src="https://res.cloudinary.com/dlk7w16gw/image/upload/v1679750199/lh8w2wisuxplzgosrgy3.png"/></Nav.Link>
                                        <>
                                            <Modal size="lg" show={show} onHide={handleClose}>
                                                <Modal.Header closeButton>
                                                <Modal.Title>Cart Items</Modal.Title>
                                                </Modal.Header>
                                                
                                                <Modal.Body><MyCart /></Modal.Body> 
                                                <Modal.Body className="pt-0 mt-0 text-end">
                                                <Button variant="primary" as={Link} to="/products" onClick={handleClose}>Add Item</Button>
                                                </Modal.Body>
                                            </Modal>
                                        </>
                                    </>  
                                }
                                <Dropdown className="m-0 p-2 nav-link nav-font">
                                    <Dropdown.Toggle className="nav-font" variant="light" id="dropdown-basic">
                                    {firstName} {lastName}
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className="text-center">
                                        <img style={{ height: '3rem'}} src="https://res.cloudinary.com/dlk7w16gw/image/upload/v1679878483/bke0h8bmffwqhonyaxup.png"/>
                                        <p className="py-0">{firstName} {lastName}</p>
                                        <p className="py-0">{email}</p>
                                        <p className="py-0">{mobileNum}</p>
                                        <Dropdown.Item  className="nav-font"  ><Nav.Link className="nav-font" as={ NavLink } to='/updateProfile' >Update User Profile</Nav.Link></Dropdown.Item>
                                        { !user.isAdmin ? 
                                            <Dropdown.Item  className="nav-font" ><Nav.Link as={ NavLink } to='/orderHistory' >View Order History</Nav.Link></Dropdown.Item>
                                        :
                                            <>
                                            </>
                                        
                                        }
                                        <Dropdown.Divider />
                                        <Dropdown.Item ><Nav.Link className="nav-font" as={ NavLink } to='/logout' >Logout</Nav.Link></Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>  
                            </>
                            :
                            <>
                                <Nav.Link as={ NavLink } to='/login' className="nav-font" >Login</Nav.Link>
                                <Nav.Link as={ NavLink } to='/register' className="nav-font" >Register</Nav.Link>
                            </>
                        } 
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}